export const ADD_TODO = 'ADD_TODO'
export const DELETE_TODO = 'DELETE_TODO'
export const CLEAR_COMPLETED_TODOS = 'CLEAR_COMPLETED_TODOS'
export const TOGGLE_TODO = 'TOGGLE_TODO'

export const addTodo=  (text) =>({type:ADD_TODO, payload:({text}) })
export const deleteTodo=  (text) =>({type:DELETE_TODO, payload:({text}) })
export const clearCompletedTodos=  (text) =>({type:CLEAR_COMPLETED_TODOS, payload:({text}) })
export const toggleTodo=  (text) =>({type:ADD_TODO, payload:({text}) })