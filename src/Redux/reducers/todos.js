import {todos as intialTodos} from '../../todos.js'
import {ADD_TODO,DELETE_TODO,CLEAR_COMPLETED_TODOS,TOGGLE_TODO} from '../actions/todosActions'
import {v4 as uuid} from 'uuid'
export const todos = (state=intialTodos,action) =>{
    switch(action.type){
        case ADD_TODO:{
            const newId = uuid()
            const newTodo = {
            "userId": 1,
            "id": newId,
            "title": action.payload.text,
            "completed": false
            }
            
           return {
                ...state,[newId]:newTodo
            }
           
        }   case DELETE_TODO:{

            }
            case CLEAR_COMPLETED_TODOS:{

            }
            case TOGGLE_TODO:{

            }
        default: return state
    }
}