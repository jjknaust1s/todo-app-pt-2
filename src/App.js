import React, { Component } from "react";
import todosList from "./todos.json";
import TodoList from './components/todoList/TodoList'
// import TodoItem from "./components/todoItem/TodoItem";
import Footer from './components/footer/Footer'
import './App.css';
import { Switch,Route } from "react-router-dom";
import {connect} from 'react-redux'
import {addTodo} from './Redux/actions/todosActions'
import {v4 as uuid} from 'uuid'


class App extends Component {
  
  state = {
    todos: todosList,
    text: ''
  };
     handleEnterInput = (event)  => {
    if(event.keyCode === 13){
      const newId = uuid()
      let newTodoItem = event.target.value
      let newTodo = {userId: 1, id: newId, title: newTodoItem, completed: false}
      this.props.addTodo(newTodoItem)
this.setState(state =>{
  return {
   todos: [newTodo, ...state.todos],
   text: ''
  }
})
    }
   
    }
    handleChange = event => {
        this.setState({
          text: event.target.value
        });
    };

    handleCheckOff = id => {
      console.log(id)
      let newTodos =  this.state.todos.map(todo =>{
        if(todo.id=== id){
          return{
            ...todo, completed: !todo.completed
          }
        }
        return {
          ...todo
        }
      }) 
      this.setState({
        todos: newTodos
      })
      }
      
      handleDelete= (todoId) => {
        const newTodos = this.state.todos.filter(
          todoItem => todoItem.id !==todoId
        )
        this.setState({
          todos: newTodos
        })
      }
      handleDeleteCompleted=()=>{
      const newTodos = this.state.todos.filter(
        todoItem => todoItem.completed !==true
      )
      this.setState({
        todos: newTodos
      })

    }
  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input 
          onChange={this.handleChange}
          onKeyDown ={this.handleEnterInput} 
          value= {this.state.text}
          className="new-todo" 
          placeholder="What needs to be done?" 
          autoFocus 
          />
        </header>
        <Switch>
  <Route 
    exact 
    path='/' 
    render= {(props)=> <TodoList
      todos={this.state.todos} 
      handleCheckOff={this.handleCheckOff} 
      handleDelete ={this.handleDelete}
      handleDeleteCompleted ={this.handleDeleteCompleted}
       />}/>
  <Route 
    exact 
    path='/active' 
    render= {(props)=> <TodoList
      todos={this.state.todos.filter(
        todoItem => todoItem.completed !==true
      )} 
      handleCheckOff={this.handleCheckOff} 
      handleDelete ={this.handleDelete}
      handleDeleteCompleted ={this.handleDeleteCompleted}
       />}/>
       <Route 
    exact 
    path='/completed' 
    render= {(props)=> <TodoList
      todos={this.state.todos.filter(
        todoItem => todoItem.completed ===true
      )} 
      handleCheckOff={this.handleCheckOff} 
      handleDelete ={this.handleDelete}
      handleDeleteCompleted ={this.handleDeleteCompleted}
       />}/>
        </Switch>
    <Footer 
    todos={this.state.todos} 
    handleDeleteCompleted={this.handleDeleteCompleted}  
    />
        
      </section>
    );
  }
}
const mapStateToProps = (state) =>({
  todos:state.todos
})
const mapDispatchToProps =(dispatch)=>({
  addTodo: text => dispatch(addTodo(text))
})
export default connect(mapStateToProps,mapDispatchToProps)(App);

