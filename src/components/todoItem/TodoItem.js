import React,{ Component} from 'react'
class TodoItem extends Component {
  
    render() {
      return (
        <li className={this.props.completed ? "completed" : ""}>
          <div className="view">
            <input 
            onChange={(event)=> this.props.handleCheckOff(this.props.id)} 
            className="toggle" 
            type="checkbox" 
            checked={this.props.completed} />
            <label>{this.props.title}</label>
            <button 
             onClick={(event)=> this.props.handleDelete(this.props.id)} 
            className="destroy" />
          </div>
        </li>
      );
    }
}
export default TodoItem