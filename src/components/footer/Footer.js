import React, {Component }from 'react'
import { NavLink } from 'react-router-dom';
class Footer extends Component{
    render(){
         return(
<footer className="footer">
    <span className="todo-count">
         <strong>
           {this.props.todos.filter(todoItem => todoItem.completed !==true).length}
           </strong> item(s) left
    </span>
<ul className="filters">
    <li>
      <NavLink exact to='/' 
      activeClassName="selectedLink"
      >All</NavLink> 
    </li>
    <li>
      <NavLink to="/active" activeClassName="selectedLink">Active</NavLink>
    </li>
    <li>
      <NavLink to="/completed" activeClassName="selectedLink">Completed</NavLink>
    </li>
  </ul>
<button 
onClick={this.props.handleDeleteCompleted} 
className="clear-completed"
>
Clear completed
</button>
</footer> 
)
   
}
    }
   
export default Footer